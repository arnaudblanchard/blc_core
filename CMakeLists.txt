# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (2011 - 2016)
# Author: Arnaud Blanchard
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured  and, more generally, to use and operate it in the same conditions as regards security.
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms. 

cmake_minimum_required(VERSION 2.6)
project(blc_core)
add_definitions(-Wextra -Wall)

#source files
set(blc_sources 
	src/blc_text.cpp 
	src/blc_tools.cpp 
	src/blc_mem.cpp		
)
include_directories(include)

#By default only the shared version is generated
add_library(shared_blc SHARED ${blc_sources})
add_library(static_blc STATIC ${blc_sources})

set_target_properties(shared_blc_core PROPERTIES OUTPUT_NAME blc_core)
set_target_properties(static_blc_core PROPERTIES OUTPUT_NAME blc_core)
	
#Building examples
#add_executable(blc_demo examples/blc_demo.c)
#target_link_libraries(blc_demo shared_blc)

#Add a target to generate documentation
find_package(Doxygen)
if(DOXYGEN_FOUND)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doxyfile.in doxyfile.doxy) #Replace the CMAKE variables in the Doxyfile
add_custom_target(doc_${PROJECT_NAME} ${DOXYGEN_EXECUTABLE} doxyfile.doxy COMMENT "Generating API documentation with Doxygen" VERBATIM )
else(DOXYGEN_FOUND)
message("You need to install doxygen to generate the doc.")
endif(DOXYGEN_FOUND)

