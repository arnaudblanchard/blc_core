
Sparse checkout
---------------

Because blc_core is a subpart of blc, we only checkout a subpart of blc

- enable the sparse checkout option (git config core.sparsecheckout true)
- adding what you want to see in the .git/info/sparse-checkout file
- re-reading the working tree to only display what you need
