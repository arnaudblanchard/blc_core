Building blc_core
=================

     cd build
     cmake ..
     make -j    
     
 `libblc_core.{so|dylib}` and `libblc.a` will be in `build/`
 
  
     